# eZ Platform 3.0 Preview SiteAccess Matcher Bundle
eZ Platform 3.0 bundle which sets correct default siteaccess in admin preview.

## Installation
- Run `composer require`:

```bash
$ composer require contextualcode/ezplatform-preview-siteaccess-matcher-bundle
```

- Enable this bundle in `config/bundles.php` by adding this line:

```php
    return [
        ...,
        ContextualCode\EzPlatformPreviewSiteAccessMatcherBundle\ContextualCodeEzPlatformPreviewSiteAccessMatcherBundle::class => ['all' => true],
    ];
```

And then add this to the bottom of your `app/config/routing.yml`:

```yml
contextual_code_ezplatform_preview_siteaccess_matcher:
    resource: "@ContextualCodeEzPlatformPreviewSiteAccessMatcherBundle/Resources/config/routing.yml"
```
